package com.example.jeanclaude.tpstartactivity4solutions

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.RatingBar
import android.widget.Toast

class Suite : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.suite)

        // c'est juste pour montrer que le onPause est appelé après que
        // l'activité soit passée en arrière-plan (et après une rotation
        // d'écran)
        Toast.makeText(applicationContext, "on Create", Toast.LENGTH_SHORT)
                .show()

        /* (ne pas enlever les commentaires)
         * plutôt que d'initialiser les composants IHM ici, on le fait dans le
		 * onStart (meilleur)
		 *
		 *

        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar1);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Intent intent = getIntent();
                intent.putExtra("vote", rating);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        if (getIntent().getExtras() != null) {
            float vote =
                    getIntent().getExtras().getFloat("vote_initial");
            ratingBar.setRating(vote);
        }
        */
    }

    override fun onStart() {
        // TODO Auto-generated method stub
        super.onStart()
        val ratingBar = findViewById<View>(R.id.ratingBar1) as RatingBar

        // solution 4

        // astuce :
        // si on met ce code APRES le listener, le fait de faire un setRating
        // équivaut à faire un clic sur la ratingBar donc cela déclenche
        // automatiquement le listener et donc on n'a pas le temps de voter en
        // cliquant sur la ratingBar !
        // SAUF si on gère le "fromUser" !
        if (intent.extras != null) {
            val vote = intent.extras!!.getFloat("vote_initial")
            ratingBar.rating = vote
        }

        ratingBar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser ->
            // solution 2, 3, 4
            val intent = intent

            // solution 3
            intent.putExtra("vote", rating)

            // solution 2, 3
            setResult(Activity.RESULT_OK, intent)

            // solution 1, 2, 3, 4
            if (fromUser)
            // solution 1
                finish()
        }

        // solution 4
        if (intent.extras != null) {
            val vote = intent.extras!!.getFloat("vote_initial")
            ratingBar.rating = vote
        }
    }

    // Laisser cette méthode commentée pour le premier test
    // puis la décommenter pour montrer l'intérêt du "fromUser" pour la ratingBar
    override fun onResume() {
        // TODO Auto-generated method stub

        // c'est juste pour montrer que le onResume est appelé après le
        // onCreate (y compris après une rotation de l'écran !)
        super.onResume()
        val ratingBar = findViewById<View>(R.id.ratingBar1) as RatingBar

        // chaque fois on augmente de 1
        // ATTENTION : le setRatings équivaut à faire un clic sur la ratingBar donc cela déclenche
        // automatiquement le listener et donc on n'a pas le temps de voter en cliquant sur la ratingBar !
        // SAUF si on gère le "fromUser" !
        ratingBar.rating = ratingBar.rating + 1
    }

    override fun onPause() {
        // TODO Auto-generated method stub

        // c'est juste pour montrer que le onPause est appelé après que
        // l'activité soit passée en arrière-plan (et après une rotation
        // d'écran)
        super.onPause()
        Toast.makeText(applicationContext, "on Pause", Toast.LENGTH_SHORT)
                .show()
    }

    override fun onStop() {
        // TODO Auto-generated method stub

        // c'est juste pour montrer que le onPause est appelé après que
        // l'activité soit passée en arrière-plan (et après une rotation
        // d'écran)
        super.onStop()
        Toast.makeText(applicationContext, "on Stop", Toast.LENGTH_SHORT)
                .show()
    }

    // solution 2, 3, 4
    override fun onBackPressed() {
        // TODO Auto-generated method stub
        //super.onBackPressed();
        val intent = intent
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

}
