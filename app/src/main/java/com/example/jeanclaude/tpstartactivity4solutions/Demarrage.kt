package com.example.jeanclaude.tpstartactivity4solutions

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast

class Demarrage : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.demarrage)
    }

    fun lance_activite(v: View) {
        //val intent = Intent(applicationContext, Suite::class)
        val intent = Intent(applicationContext,Suite::class.java)

        //pour solution 4 (passage de paramètre)
        val vote_initial = 2f
        intent.putExtra("vote_initial", vote_initial)

        // pour solution 2/3/4
        startActivityForResult(intent, MONCODE)

        //        // pour solution 1 uniquement
        //        startActivity(intent);
    }

    // pour solution 2/3/4
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // super.onActivityResult(requestCode, resultCode, data);
        val vote: Float

        when (requestCode) {
            MONCODE -> when (resultCode) {
                Activity.RESULT_OK -> {
                    // pour solution 2, 3 et 4
                    Toast.makeText(applicationContext, "Merci d'avoir voté",
                            Toast.LENGTH_SHORT).show()
                    // pour solution 3 et 4
                    vote = data!!.getFloatExtra("vote", -100f)
                    Toast.makeText(applicationContext,
                            "vous avez voté $vote", Toast.LENGTH_SHORT).show()
                }
                Activity.RESULT_CANCELED -> {
                    // pour solution 2, 3 et 4
                    Toast.makeText(applicationContext,
                            "Pourquoi ne pas avoir voté ?", Toast.LENGTH_SHORT)
                            .show()
                    // pour solution 3 et 4
                    if (data != null) {
                        vote = data.getFloatExtra("vote", -100f)
                        Toast.makeText(applicationContext,
                                "vote pas ok, cela vaut $vote",
                                Toast.LENGTH_SHORT).show()
                    } else
                        Toast.makeText(applicationContext,
                                "vous n'avez pas voté", Toast.LENGTH_SHORT).show()
                }
                else -> {
                }
            }

            else -> {
            }
        }
    }

    companion object {

        // pour solution 2/3/4
        private val MONCODE = 10
    }
}
